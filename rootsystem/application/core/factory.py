#TODO eliminar exec reemplazar por __import__(ver xfc)
from os import system


class Factory(object):

    def make(cls, class_name, objid):
        clslower = class_name.lower()
        exec "from modules.{} import {}".format(clslower, class_name)
        obj = locals()[class_name]()
        setattr(obj, "{}_id".format(clslower), objid)
        obj.select()
        return obj
