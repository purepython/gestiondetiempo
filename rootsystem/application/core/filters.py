class Filter:

    @staticmethod
    def sanitize_html_tags(string):
        return string.replace("<", "&lt;").replace(">", "&gt;")
    
    @staticmethod
    def sanitize_quotes(string):
        return string.replace("'", "&#39;").replace('"', "&#34;")

    @staticmethod
    def sanitize_string(string):
        string = string.replace("&", "&#38;")
        string = Filter.sanitize_html_tags(string)
        string = Filter.sanitize_quotes(string)
        string = string.replace("#", "&#35;")
        string = string.replace("?", "&#63;")
        return string

    @staticmethod
    def sanitize_int(value):
        try:
            value = int(value)
        except:
            value = 0
        return value
