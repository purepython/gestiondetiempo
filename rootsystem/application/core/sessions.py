from Cookie import SimpleCookie
import hashlib, time, uuid, shelve # TODO refactorizar
from logging import basicConfig, WARNING, warning
from os import environ, unlink
from os.path import isfile
from subprocess import Popen, PIPE

from core.headers import Header
from settings import SESS_DIR, LOGIN_PAGE, SERVER_NAME, HTTP_HTML, \
    HTTP_REDIRECT, RESTRICTED_PAGE, URI, DEFAULT_RESOURCE, PRIVATE_DIR


class Sessions(object):

    @staticmethod
    def set(varname, value):
        """
        Set a new session variable
        """
        sess_file = SessionsHelper.get_filename()
        if not isfile(sess_file):
            raise UserWarning("'set' called before create session")
        sess_file = Sessions.get_all()
        sess_file[varname] = value
        sess_file.close

    @staticmethod
    def get_all():
        """
        Return all variable sessions
        """
        sid = SessionsHelper.get_sid()
        session = shelve.open('{}/sess_{}'.format(SESS_DIR, sid))
        return session

    @staticmethod
    def get(varname):
        """
        Return a single variable session
        """
        shelf = Sessions.get_all()
        value = shelf[varname] if varname in shelf else ''
        shelf.close()
        return value

    @staticmethod
    def start(tls=False):
        """
        Start a user session system

        Arguments:
        tls -- Set secure argument for HTTPS

        This function destroy all previous sessions and
        make a cookie file with an unique SID
        """
        Sessions.destroy()
        sid = SessionsHelper.set_sid()
        Cookies.create(sid, tls=tls)

    @staticmethod
    def create():
        """
        Create the session file using shelve
        """
        sid = SessionsHelper.get_sid()
        if sid == 0: SessionsHelper.redirect()
        sess_file = SessionsHelper.get_filename()
        cookie_file = sess_file.replace('sess_', '')
        if not isfile(cookie_file): SessionsHelper.redirect()
        command = ['rm', '-f', cookie_file]
        Popen(command, stdout=PIPE, stderr=PIPE)
        shelve.open(sess_file).close()
        Sessions.set_session_vars()

    @staticmethod
    def destroy():
       """
       Destroy session file and user cookie
       """
       sid = SessionsHelper.get_sid()
       sess_file = '{}/sess_{}'.format(SESS_DIR, sid)
       if isfile(sess_file): unlink(sess_file)
       Cookies.destroy()

    @staticmethod
    def is_active_session():
        """
        This function look if the session file exist
        Return False if the session file doesn't exist
        """
        cache_control = "Cache-Control: no-cache,no-store"
        pragma = "Pragma: no-cache"
        headers = SessionHeaders()
        headers.append(cache_control)
        headers.append(pragma)
        headers.send()
        sid = SessionsHelper.get_sid()
        sess_file = '{}/sess_{}'.format(SESS_DIR, sid)
        return False if not isfile(sess_file) else True

    @staticmethod
    def check():
        """
        If the session file doesn't exist, this function
        redirect the user to the login page.

        Other wise check if the session has anomalies
        """
        if not Sessions.is_active_session():
            SessionsHelper.redirect()
        Sessions.check_anomalies()

    @staticmethod
    def check_level(level):
        #Sessions.check_anomalies()
        session_level = Sessions.get('LEVEL')
        if not session_level <= level:
            SessionsHelper.redirect(RESTRICTED_PAGE)

    @staticmethod
    def update():
        sess_file = SessionsHelper.get_filename()
        if isfile(sess_file):
            command = ['touch', sess_file]
            Popen(command, stdout=PIPE, stderr=PIPE)

    @staticmethod
    def check_authentication():
        if URI == LOGIN_PAGE and Sessions.is_active_session():
            SessionsHelper.redirect(DEFAULT_RESOURCE)

    @staticmethod
    def set_session_vars():
        session_vars = ['HTTP_USER_AGENT', 'REMOTE_ADDR',
                        'HTTP_ACCEPT_ENCODING']
        for v in session_vars:
            Sessions.set(v.replace('HTTP_', ''), environ[v])

    @staticmethod
    def check_anomalies():
        user_agent = (environ['HTTP_USER_AGENT'] == Sessions.get('USER_AGENT'))
        ip = (environ['REMOTE_ADDR'] == Sessions.get('REMOTE_ADDR'))
        accept_encoding = (environ['HTTP_ACCEPT_ENCODING'] ==
                           Sessions.get('ACCEPT_ENCODING'))

        problems = []
        if user_agent == False: problems.append('USER_AGENT')
        if ip == False: problems.append('REMOTE_ADDR')
        if accept_encoding == False: problems.append('ACCEPT_ENCODING')

        if not user_agent or not (ip or accept_encoding):
            for problem in problems: SessionsLogger.log(problem)
            headers = SessionHeaders()
            headers.append(headers.BAD_REQUEST + '\n')
            headers.send()
            exit()


class SessionsHelper(object):

    @staticmethod
    def clear(sid):
        lista = [char for char in sid if char.isalnum()]
        sid = ''.join(lista)
        return sid if len(sid) == 32 else '0'

    @staticmethod
    def redirect(page=LOGIN_PAGE):
        url = 'http://{}{}'.format(SERVER_NAME, page)
        print "Location: {}\n".format(url)
        exit()

    @staticmethod
    def get_sid():
        cookie = SimpleCookie()
        cookie_string = environ.get('HTTP_COOKIE', '')
        cookie.load(cookie_string)
        sid = cookie['__PYSESSION__SID'].value if '__PYSESSION__SID' in cookie else '0'
        return SessionsHelper.clear(sid)

    @staticmethod
    def get_filename():
        sid = SessionsHelper.get_sid()
        sess_file = '{}/sess_{}'.format(SESS_DIR, sid)
        return sess_file

    @staticmethod
    def set_sid():
        random_num = hashlib.md5(str(uuid.uuid4())).hexdigest()
        user_ip = hashlib.sha224(environ.get('REMOTE_ADDR')).hexdigest()
        hora = hashlib.sha512(time.asctime(time.localtime(time.time()))).hexdigest()
        sid = hashlib.md5('{}{}{}'.format(random_num, user_ip, hora)).hexdigest()
        return sid


class Cookies(object):

    @staticmethod
    def create(sid, expires=43200, tls=False):
        tls = 'secure' if tls else ''
        cookie = "Set-Cookie: __PYSESSION__SID={}; Max-Age={};".format(sid, expires)
        cookie += " Path=/; HttpOnly; SameSite=Strict; {}".format(tls)
        headers = SessionHeaders()
        headers.append(cookie)
        headers.send()
        # TODO Refactoring cookies create temp file
        command = ["touch", "{}/{}".format(SESS_DIR, sid)]
        Popen(command, stdout=PIPE, stderr=PIPE)

    @staticmethod
    def destroy():
        cookie = "Set-Cookie: __PYSESSION__SID=0; Max-Age=1;"
        cookie += " Path=/; HttpOnly; SameSite=Strict;"
        print cookie

    @staticmethod
    def get(name):
        cookie = SimpleCookie()
        cookie_string = environ.get('HTTP_COOKIE', '')
        cookie.load(cookie_string)
        return cookie[name].value if name in cookie else ''


class SessionHeaders(Header):

    def send(self):
        print '\n'.join(self)
        self[:] = []


class SessionsLogger(object):

    @staticmethod
    def log(problem):
        mensaje = "[%(asctime)s] [{ip}] [{agent}] [{uri}] [{script}] "
        mensaje += "SIDANOMALY: anomalie detected in {problem} ({retrived}"
        mensaje += " is not equal to {received})"
        k = 'HTTP_{}'.format(problem) if problem != 'REMOTE_ADDR' else problem
        dic = dict(
            ip=environ['REMOTE_ADDR'],
            agent=environ['HTTP_USER_AGENT'],
            script=environ['SCRIPT_FILENAME'],
            uri=URI,
            problem=problem,
            retrived=Sessions.get(problem),
            received=environ[k]
        )
        basicConfig(filename='{}/pysessions.log'.format(PRIVATE_DIR), level=WARNING,
                    format="%(message)s")
        warning(mensaje.format(**dic))


class SessionAnomalyChecker:

    @staticmethod
    def is_sid_modified(sid):
        array = [char for char in sid if char.isalnum()]
        clean_sid = ''.join(array)
        hashed_clean_sid = hashlib.sha1(clean_sid).hexdigest()
        hashed_sid = hashlib.sha1(sid).hexdigest()
        if hashed_sid != hasehd_clean_sid: return True

    @staticmethod
    def has_too_many_cookies():
        cookies = environ.get('HTTP_COOKIE', '').count('__PYSESSION__')
        if cookies > 1: return True

    @staticmethod
    def is_not_sid():
        if not Cookies.get('__PYSESSION__SID'): return True

