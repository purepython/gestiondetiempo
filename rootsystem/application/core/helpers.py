# -*- coding: utf-8 -*-
from datetime import date, time
import re
from settings import APP_ERRORS, STATIC_DIR


class FilesHelper:

    @staticmethod
    def read_file(filename):
        """Read file
                Params:
                        filename -- (string) File path to be read
                Returns:
                        content -- (string) Text string with the content read
        """
        file_path = '{}/html/{}.html'.format(STATIC_DIR, filename)
        with open(file_path, 'r') as tmp:
                content = tmp.read()
        return content


class Sanitizer:

    @staticmethod
    def getval(form, key):
        def wrapper():
            return form[key].value if key in form else ''
        return '' if form is None else wrapper()

    @staticmethod
    def clean_email(email):
        """Eval email format
				Params:
						email -- (string) to be cleaned
				Returns:
						string -- cleaned
        """
        new_email = email
        valid_chars = ["@", ".", "-", "_", "+"]
        for char in email:
                if not char.isalnum() and not char in valid_chars:
                        new_email = new_email.replace(char, "")
        if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',new_email.lower()):
                return new_email
        else:
                return False

	@staticmethod
	def ucwords(text):
		uc_text = ''
		for id_c, char in enumerate(text):
			if char in whitespace or not char.isalpha():
				uc_text += char
			elif char.isalpha() and (not id_c or text[id_c - 1] in whitespace):
				uc_text += char.upper()
			else:
				uc_text += char
		return uc_text


class Validator:

    @staticmethod
    def is_null(module, name, value):
        if not value:
            return APP_ERRORS[module][name]

# =================================================================================================
#                                          ALIAS
# =================================================================================================
def read_file(filename):
    return FilesHelper.read_file(filename)

def ucwords(text):
	return Sanitizer.ucwords(text)


