class Controller(object):

    clase = self.__class__.__name__.replace("Controller", "")

    def __init__(self):
        self.model = getattr(self, self.clase)()
        self.view = getattr(self, "{}View".format(self.clase))()
