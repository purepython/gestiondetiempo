CREATE TABLE IF NOT EXISTS usuario(
    usuario_id VARCHAR(32) NOT NULL PRIMARY KEY,
    denominacion VARCHAR(45),
    nivel INT(2),
    email VARCHAR(70)
)ENGINE=InnoDB;

-- Usuario admin desarrollo
INSERT IGNORE INTO usuario (usuario_id, denominacion, nivel, email) 
VALUES ('574b8603471fd44d366a3db64dd0a4ef', 'Administrador', 1, 'admin@admin.com');

CREATE TABLE IF NOT EXISTS actividad(
    actividad_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(80)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS empresa(
    empresa_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(80)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS tarea(
    tarea_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(80),
    hora_inicio DATETIME,
    hora_fin DATETIME,
    empresa INT(11),
    FOREIGN KEY (empresa)
        REFERENCES empresa (empresa_id)
        ON DELETE SET NULL,
    actividad INT(11),
    FOREIGN KEY (actividad)
        REFERENCES actividad (actividad_id)
        ON DELETE SET NULL,
    usuario VARCHAR(32),
    FOREIGN KEY (usuario)
        REFERENCES usuario (usuario_id)
        ON DELETE SET NULL
)ENGINE=InnoDB;
