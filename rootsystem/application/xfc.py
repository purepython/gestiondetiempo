#!/usr/bin/env python
#-*- coding: utf-8 -*-
from os.path import isfile
from settings import *
from core.sessions import Sessions

Sessions.update()
Sessions.check_authentication()

#Para la comprobación de C y m
error_module = error_resource = True

# Implementación de (m ∈ M ) ⇐⇒ C
if isfile(MODULE_PATH):
    modulo = __import__(PACKAGE, fromlist=[CONTROLLER])
    controller = getattr(modulo, CONTROLLER)()
    error_module = False

# Implementación de (r ∈ C) ⇐⇒ r
if not error_module and hasattr(controller, RESOURCE):
    getattr(controller, RESOURCE)()
    error_resource = False

# Implementación de:
# err(Rq) ⇒ location(r n ) ∨ (set(st, 404) ∧ set(M IM E))
if error_module or error_resource:
    if SHOW_ERROR_404:
        print "Status: 404 Not Found"
        print ""
        exit()

    print HTTP_REDIRECT
    #print error_header if SHOW_ERROR_404 else HTTP_REDIRECT

