from os import environ

DB_HOST = 'localhost'
DB_USER = 'gestiondetiempo'
DB_PASS = 'gestiondetiempo'
DB_NAME = 'gestiondetiempo'

DOCUMENT_ROOT = '/srv/websites/purepython/gestiondetiempo/rootsystem'
PRIVATE_DIR = DOCUMENT_ROOT.replace('rootsystem', 'private')
TMP_DIR = "/tmp"
LOG_FILE = '{}/logs/gestiondetiempo.log'.format(PRIVATE_DIR)

DEFAULT_RESOURCE = "/admin/intro"
SHOW_ERROR_404 = False  # Produccion, muestra el recurso por defualt en la raiz
STATIC_DIR = "{}/static".format(DOCUMENT_ROOT)
TEMPLATE_FILE = "{}/html/template.html".format(STATIC_DIR)

# Directorio de sesiones (crear a mano si no existe)
SESS_DIR = "{}/pysessions".format(PRIVATE_DIR)
LOGIN_PAGE = "/credencial/login"

# Pagina de acceso restringido
RESTRICTED_PAGE = "/page/restricted-page"

# Diccionario de diccionarios para el manejo de los mensajes de error
APP_ERRORS = dict()

APP_ERRORS['VISITA'] = dict(
	direccion='La direccion no puede estar vacia',
	nota='Especifique por favor el motivo de la visita',
	identificacion='Es necesario registrar una identificacion para permitir el acceso',
	placa_img='Es necesario tomar una foto de la placa del auto que esta registrando',
	placa_txt='Anote el numero de la placa que corresponde al auto que estaa registrando'
)

APP_ERRORS['USUARIO'] = dict(
    usuario='El usuario esta duplicado'
)

