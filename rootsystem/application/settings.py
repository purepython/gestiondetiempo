#!/usr/bin/env python
from os import environ
from config import *

URI = environ['REQUEST_URI'] if 'REQUEST_URI' in environ else '/'
URI_LIST = URI.split('/')[1:]
MODULE = URI_LIST[0].replace('.', '')
PACKAGE = "modules.{}".format(MODULE)
MODULE_PATH = "modules/{}.py".format(MODULE)
CONTROLLER = "{}Controller".format(MODULE.title())
RESOURCE = URI_LIST[1].replace('-', '_') if len(URI_LIST) > 1 else ''
ARG = URI_LIST[2].split('?')[0] if len(URI_LIST) > 2 else 0

HTTP_404 = "Status: 404 Not Found\n"
HTTP_HTML = "Content-type: text/html; charset=utf-8\n"
VARIABLE_MIME = "Content-type: {}\n"
SERVER_NAME = environ['SERVER_NAME'] if 'SERVER_NAME' in environ else ''
REDIRECT_URL = "http://{}{}".format(SERVER_NAME, DEFAULT_RESOURCE)
HTTP_REDIRECT = "Location: {}\n".format(REDIRECT_URL)
