#-*- coding: utf-8 -*-
from core.helper import read_file
from core.mvc.view import View

class PageView(View):

    def restricted_page(self):
        html = read_file('back/restricted_page')
        print self.render_template('Pagina restringida', html, 'back')


class PageController(object):

    def __init__(self):
        self.view = PageView()

    def restricted_page(self):
        self.view.restricted_page()
