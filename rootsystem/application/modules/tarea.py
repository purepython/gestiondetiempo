# -*- coding: utf-8 -*-

from os import environ, listdir
import smtplib
from cgi import FieldStorage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import unlink
from os.path import isfile
from string import Template

from core.composite import Composite
from core.collector import CollectorObject
from core.db import DBQuery
from core.filters import Filter
from core.headers import header
from core.helper import read_file, Sanitizer
from core.mvc.view import View
from core.sessions import Sessions
from core.server import ServerFile
from core.stdobject import StdObject

from modules.actividad import Actividad, ActividadController
from modules.empresa import Empresa, EmpresaController
from modules.usuario import Usuario, UsuarioController
from settings import PRIVATE_DIR, ARG, HTTP_HTML, MODULE, RESOURCE


class Tarea(StdObject):

    def __init__(self, usuario=None, empresa=None, actividad=None):
        self.tarea_id = 0
        self.titulo = ''
        self.hora_inicio = ''
        self.hora_fin = ''
        self.usuario = Composite('Usuario').compose(usuario)
        self.empresa = Composite('Empresa').compose(empresa)
        self.actividad = Composite('Actividad').compose(actividad)

    def insert(self):
        sql = """
            INSERT INTO     tarea
                            (titulo, hora_inicio, hora_fin, usuario, empresa, actividad)
            VALUES          ('{}', '{}', '{}', '{}', {}, {})
        """.format(
            self.titulo,
            self.hora_inicio,
            self.hora_fin,
            self.usuario.usuario_id,
            self.empresa.empresa_id,
            self.actividad.actividad_id
        )
        self.tarea_id = DBQuery().execute(sql)

    def update(self):
        sql = """
            UPDATE      tarea
            SET         titulo = '{}', hora_inicio = '{}', hora_fin = '{}',
			empresa = {}, actividad = {}
            WHERE       tarea_id = {}
        """.format(
            self.titulo,
            self.hora_inicio,
            self.hora_fin,
            self.empresa,
            self.actividad,
            #usuario?
            self.tarea_id
        )
        DBQuery().execute(sql)

    def delete(self):
        super(Tarea, self).delete()
        # Elminar archivo de la tarea
        archivo = '{}/{}/tarea_{}'.format(PRIVATE_DIR, MODULE, self.tarea_id)
        if isfile(archivo): unlink(archivo)


class TareaView(View):

    def agregar(self, empresas, actividades):
        html = read_file('back/agregar_tarea')
        r_empresa = self.render_regex(html,'empresas', empresas)
        content = self.render_regex(r_empresa,'actividades', actividades)
        print self.render_template('Agregar tarea', content, 'back')

    def listar(self, tareas):
        html = read_file('back/listar_tarea')
        contenido = self.render_regex(html, 'tareas', tareas)
        print self.render_template('Listado de tareas', contenido, 'back')

    def ver(self, modelo):
        diccionario = TareaHelper.set_dict(modelo)
        html = read_file('back/ver_tarea')
        content = Template(html).safe_substitute(diccionario)
        print self.render_template("Ver tarea", content, 'back')

    def wait(self, url, message):
        print self.render_wait(url, 1, message)


class TareaController(object):

    def __init__(self):
        self.model = Tarea()
        self.view = TareaView()

    def agregar(self):
        Sessions.check()
        ec = CollectorObject()
        ec.get('Empresa')

        ac = CollectorObject()
        ac.get('Actividad')
        self.view.agregar(ec.collection, ac.collection)

    def guardar(self):
        Sessions.check()
        form = FieldStorage()

        if not 'empresa' in form or not 'actividad' in form:
            print "Location: http://gestiondetiempo.com/tarea/agregar/"
            print ""
            exit()

        # Recibir valores desde el formulario
        titulo = Sanitizer.getval(form, 'titulo')
        empresa = Sanitizer.getval(form, 'empresa')
        actividad = Sanitizer.getval(form, 'actividad')
        hora = Sanitizer.getval(form, 'hora').split('-')
        hora_inicio = hora[0]
        hora_fin = hora[1]

        # Sanear datos simples
        titulo = Filter.sanitize_string(titulo)
        hora_inicio = Filter.sanitize_string(hora_inicio)
        hora_fin = Filter.sanitize_string(hora_fin)

        # Guardamos usuario
        usuario = UsuarioController()
        usuario.get(Sessions.get('UID'))

        # Guardamos empresa
        empresaobj = EmpresaController()
        empresaobj.get(empresa)

        # Guardamos actividad
        actividadobj = ActividadController()
        actividadobj.get(actividad)

        # Guardar tarea
        self.model.titulo = titulo
        self.model.hora_inicio = hora_inicio
        self.model.hora_fin = hora_fin
        self.model.empresa = empresaobj.model
        self.model.actividad = actividadobj.model
        self.model.usuario = usuario.model
        self.model.insert()
        tarea_id = self.model.tarea_id

        # Folder donde se guardan las descripciones
        folder = '{}/{}'.format(PRIVATE_DIR, MODULE)

        # Recoger el valor de la tarea y sanearlo
        tarea = Sanitizer.getval(form, 'descripcion')
        filename = 'tarea_{}'.format(tarea_id)

        # Subir tarea
        ServerFile().upload(tarea, filename, folder)
        url = '/tarea/ver/{}'.format(tarea_id)

        tarea = '{}/{}/tarea_{}'.format(PRIVATE_DIR, MODULE, tarea_id)
        with open(tarea, 'r') as f:
            self.model.tarea = f.read()
        self.view.ver(self.model)

    def ver(self):
        Sessions.check()
        self.model.tarea_id = int(ARG)
        self.model.select()
        tarea = '{}/{}/tarea_{}'.format(PRIVATE_DIR, MODULE, ARG)
        with open(tarea, 'r') as f:
            self.model.tarea = f.read()
        self.view.ver(self.model)

    def editar(self):
        self.model.tarea_id = int(ARG)
        self.model.select()
        self.view.editar(self.model)

    def actualizar(self):
        form = FieldStorage
        self.model.tarea_id = form['tarea_id'].value
        self.model.select()
        self.model.hora_inicio = form['hora_inicio'].value
        self.model.hora_fin = form['hora_fin'].value

        self.model.actividad.denominacion = form['denominacion'].value
        self.model.empresa.denominacion = form['denominacion'].value
        self.model.domicilio.update()

        self.model.update()

        self.view.wait('/tarea/ver/{}'.format(self.model.tarea_id), 'Empresa actualizada')

    def eliminar(self):
        Sessions.check()
        Sessions.check_leve(1)
        self.model.tarea_id = int(ARG)
        self.model.delete()
        self.view.wait('/tarea/listar', 'Tarea eliminada')

    def listar(self):
        Sessions.check()
        c = CollectorObject()
        c.get("Tarea")
        for t in c.collection:
            t.usuario = t.usuario.denominacion
            t.empresa = t.empresa.denominacion
            t.actividad = t.actividad.denominacion
        self.view.listar(c.collection)


class TareaHelper(object):

    @staticmethod
    def set_dict(modelo):
        propiedades = vars(modelo)
        diccionario = {}
        diccionario.update(propiedades)
        del(diccionario['usuario'])
        del(diccionario['empresa'])
        diccionario.update(vars(propiedades['usuario']))
        diccionario['usuario'] = diccionario['denominacion']
        del(diccionario['denominacion'])
        diccionario.update(vars(propiedades['empresa']))
        diccionario['empresa'] = diccionario['denominacion']
        del(diccionario['denominacion'])
        diccionario.update(vars(propiedades['actividad']))
        diccionario['actividad'] = diccionario['denominacion']
        del(diccionario['denominacion'])
        return diccionario

