from os import environ
from os.path import isfile
from cgi import FieldStorage
from hashlib import md5, sha224, sha512

from core.factory import Factory
from core.headers import header
from core.mvc.view import View
from core.helper import read_file, Sanitizer
from core.sessions import Sessions, SessionsHelper
from settings import PRIVATE_DIR, HTTP_HTML, HTTP_REDIRECT, SESS_DIR


class Credencial(object):

    def __init__(self):
        self.credencial_id = ''

    def insert(self):
        ruta = '{}/.credentials'.format(PRIVATE_DIR)
        with open('{}/.{}'.format(ruta, self.credencial_id), 'w') as f:
            f.write('')

class CredencialView(View):

    def login(self):
        html = read_file('back/login')
        header.append(header.HTML)
        header.send()
        print html


class CredencialController(object):

    def __init__(self):
        self.model = Credencial()
        self.view = CredencialView()

    def login(self):
        Sessions.start()
        self.view.login()

    def logout(self):
        Sessions.destroy()
        header.append(header.HTML)
        header.append("Refresh:0; url=/credencial/login")
        header.send()

    def validar(self):
        form = FieldStorage()
        cid = CredencialHelper.set_cid(form)
        ruta = '{}/.credentials'.format(PRIVATE_DIR)
        cid_file = '{}/.{}'.format(ruta, cid)

        if isfile(cid_file):
            uid = CredencialHelper.set_uid(form)
            usuario = Factory().make('Usuario', uid)
            Sessions.create()
            Sessions.set('UID', uid)
            Sessions.set('LEVEL', usuario.nivel)
            Sessions.set('USERNAME', usuario.denominacion)

        header.append(header.HTML)
        header.append("Refresh:0; url=/admin/intro")
        header.send()

    def guardar(self, form=None):
        Sessions.check()
        Sessions.check_level(1)

        if form is None:
            print HTTP_HTML
            exit()  # Redirigir a usuario agregar

        self.model.credencial_id = CredencialHelper.set_cid(form)
        self.model.insert()


class CredencialHelper(object):

    @staticmethod
    def set_cid(form):
        user_hash = sha224(Sanitizer.getval(form, 'usuario')).hexdigest()
        pass_hash = sha224(Sanitizer.getval(form, 'contrasena')).hexdigest()
        x = user_hash[104:116]
        salt = md5('{}{}{}'.format(user_hash, x, pass_hash)).hexdigest()
        return md5('{}{}'.format(pass_hash, user_hash, salt)).hexdigest()

    @staticmethod
    def set_uid(form):
        user_hash = sha512(Sanitizer.getval(form, 'usuario')).hexdigest()
        x = user_hash[240:266]
        salt = md5('{}{}'.format(x, user_hash)).hexdigest()
        return md5('{}{}'.format(user_hash, salt)).hexdigest()

