# -*- coding: utf-8 -*-
from os import environ, listdir
import smtplib
from cgi import FieldStorage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import unlink
from os.path import isfile
from string import Template

from core.collector import CollectorObject
from core.db import DBQuery
from core.filters import Filter
from core.headers import header
from core.helper import read_file, Sanitizer
from core.mvc.view import View
from core.sessions import Sessions
from core.server import ServerFile
from core.stdobject import StdObject
from settings import PRIVATE_DIR, ARG, HTTP_HTML, MODULE, RESOURCE


class Actividad(StdObject):

    def __init__(self):
        self.actividad_id = 0
        self.denominacion = ''

    def insert(self):
        sql = """
            INSERT INTO     actividad
                            (denominacion)
            VALUES          ('{}')
        """.format(
            self.denominacion
        )
        self.actividad_id = DBQuery().execute(sql)

    def update(self):
        sql = """
            UPDATE      actividad
            SET         denominacion = '{}'
            WHERE       actividad_id = {}
        """.format(
            self.denominacion,
            self.actividad_id
        )
        DBQuery().execute(sql)


class ActividadView(View):

    def agregar(self, coleccion):
        html = read_file('back/agregar_actividad')
        content = self.render_regex(html, 'listado', coleccion)
        print self.render_template('Agregar Actividad', content, 'back')

    def editar(self, actividad):
        html = read_file('back/editar_actividad')
        content = Template(html).safe_substitute(actividad)
        titulo = "Estas editando la actividad {}".format(actividad['denominacion'])
        print self.render_template(titulo, content, 'back')

    def wait(self, url, message):
        print self.render_wait(url, 2, message)


class ActividadController(object):

    def __init__(self):
        self.view = ActividadView()
        self.model = Actividad()

    def agregar(self):
        Sessions.check()
        Sessions.check_level(1)
        oc = CollectorObject()
        oc.get('Actividad')
        self.view.agregar(oc.collection)

    def guardar(self):
        Sessions.check()
        Sessions.check_level(1)
        form = FieldStorage()

        actividad = Sanitizer.getval(form, 'denominacion')
        actividad = Filter.sanitize_string(actividad)

        self.model.denominacion = actividad
        self.model.insert()

        self.agregar()

    def actualizar(self):
        Sessions.check()
        Sessions.check_level(1)
        form = FieldStorage()

        actividad_id = Sanitizer.getval(form, 'actividad_id')
        denominacion =  Sanitizer.getval(form, 'denominacion')

        actividad_id = Filter.sanitize_int(actividad_id)
        denominacion = Filter.sanitize_string(denominacion)

        self.model.actividad_id = actividad_id
        self.model.denominacion = denominacion
        self.model.update()
        self.agregar()

    def eliminar(self):
        self.model.actividad_id = int(ARG)
        self.model.delete()
        self.agregar()

    def editar(self):
        Sessions.check()
        Sessions.check_level(1)
        self.model.actividad_id = int(ARG)
        self.model.select()
        self.view.editar(vars(self.model))

    def get(self, objid):
        self.model.actividad_id = Filter.sanitize_int(objid)
        self.model.select()
