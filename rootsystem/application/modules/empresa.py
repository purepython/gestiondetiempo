# -*- coding: utf-8 -*-
from os import environ, listdir
import smtplib
from cgi import FieldStorage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import unlink
from os.path import isfile
from string import Template

from core.collector import CollectorObject
from core.db import DBQuery
from core.filters import Filter
from core.headers import header
from core.helper import read_file, Sanitizer
from core.mvc.view import View
from core.sessions import Sessions
from core.server import ServerFile
from core.stdobject import StdObject
from settings import PRIVATE_DIR, ARG, HTTP_HTML, MODULE, RESOURCE


class Empresa(StdObject):

    def __init__(self):
        self.empresa_id = 0
        self.denominacion = ''

    def insert(self):
        sql = """
            INSERT INTO     empresa
                            (denominacion)
            VALUES          ('{}')
        """.format(
            self.denominacion
        )
        self.empresa_id = DBQuery().execute(sql)

    def update(self):
        sql = """
            UPDATE      empresa
            SET         denominacion = '{}'
            WHERE       empresa_id = {}
        """.format(
            self.denominacion,
            self.empresa_id
        )
        DBQuery().execute(sql)


class EmpresaView(View):

    def agregar(self, coleccion):
        html = read_file('back/agregar_empresa')
        content = self.render_regex(html, 'listado', coleccion)
        print self.render_template('Agregar Empresa', content, 'back')

    def editar(self, dato_empresa):
        html = read_file('back/editar_empresa')
        content = Template(html).safe_substitute(dato_empresa)
        titulo = 'Estas editando la empresa {}'.format(dato_empresa['denominacion'])
        print self.render_template(titulo, content, 'back')

    def wait(self, url, message):
        print self.render_wait(url, 2, message)


class EmpresaController(object):

    def __init__(self):
        self.view = EmpresaView()
        self.model = Empresa()

    def agregar(self):
        Sessions.check()
        Sessions.check_level(1)
        oc = CollectorObject()
        oc.get('Empresa')
        self.view.agregar(oc.collection)

    def guardar(self):
        Sessions.check()
        Sessions.check_level(1)
        form = FieldStorage()

        empresa = Sanitizer.getval(form, 'denominacion')
        empresa = Filter.sanitize_string(empresa)

        self.model.denominacion = empresa
        self.model.insert()

        self.agregar()

    def actualizar(self):
        Sessions.check()
        Sessions.check_level(1)
        form = FieldStorage()

        # Recibimos los datos por form
        empresa_id = Sanitizer.getval(form, 'empresa_id')
        denominacion = Sanitizer.getval(form, 'denominacion')

        # Saneamos los datos
        denominacion = Filter.sanitize_string(denominacion)
        empresa_id = Filter.sanitize_int(empresa_id)

        self.model.empresa_id = empresa_id
        self.model.denominacion = denominacion
        self.model.update()

        self.agregar()


    def eliminar(self):
        Sessions.check()
        Sessions.check_level(1)
        self.model.empresa_id = int(ARG)
        self.model.delete()
        self.agregar()

    def editar(self):
        Sessions.check()
        Sessions.check_level(1)
        self.model.empresa_id = int(ARG)
        self.model.select()
        self.view.editar(vars(self.model))

    def get(self, objid):
        self.model.empresa_id = Filter.sanitize_int(objid)
        self.model.select()
