#-*- coding: utf-8 -*-
import re

from cgi import FieldStorage
from hashlib import md5, sha512
from string import Template

from core.collector import CollectorObject
from core.db import DBQuery
from core.filters import Filter
from core.helper import read_file, Sanitizer
from core.mvc.view import View
from core.sessions import Sessions

from modules.credencial import CredencialController
from settings import HTTP_HTML, ARG, APP_ERRORS, HTTP_REDIRECT


class Usuario(object):

    def __init__(self):
        self.usuario_id = ''
        self.denominacion = ''
        self.nivel = 0
        self.email = ''

    def insert(self):
        sql = """INSERT INTO usuario
                 (usuario_id, denominacion, nivel, email)
                 VALUES ('{}', '{}', {}, '{}')
              """.format(self.usuario_id, self.denominacion, self.nivel,
                         self.email)
        DBQuery().execute(sql)

    def select(self):
        sql = """SELECT usuario_id, denominacion, nivel, email
                 FROM usuario WHERE usuario_id='{}'
                 """.format(self.usuario_id)
        resultado = DBQuery().execute(sql)

        if not len(resultado) > 0:
            resultado = (('',),)

        datos = resultado[0]
        self.usuario_id = datos[0]
        self.denominacion = datos[1]
        self.nivel = datos[2]
        self.email = datos[3]

    def get_uid(self, uid):
        sql = """SELECT usuario_id 
              FROM usuario WHERE usuario_id ='{}'""".format(uid)
        return DBQuery().execute(sql)


class UsuarioView(View):

    def agregar(self, coleccion, errores={}):
        for u in coleccion:
            u.nivel = 'Administrador' if u.nivel == 1 else 'Editor'

        html = read_file('back/agregar_usuario')
        contenido = self.render_regex(html, 'listado', coleccion)
        if not errores:
            contenido = self.extract(contenido, 'errores')
        else:
            errores = '<br>'.join(errores.values())
            contenido = contenido.replace('$errores', errores)

        print self.render_template('Administrar usuarios', contenido, 'back')


class UsuarioController(object):

    def __init__(self):
        self.model = Usuario()
        self.view = UsuarioView()

    def agregar(self, errores={}):
        Sessions.check()
        Sessions.check_level(1)
        oc = CollectorObject()
        oc.get('Usuario')
        self.view.agregar(oc.collection, errores)

    def guardar(self):
        Sessions.check()
        Sessions.check_level(1)
        form = FieldStorage()
        #Recibir los datos por form
        usuario = Sanitizer.getval(form, 'usuario')
        denominacion =  Sanitizer.getval(form, 'denominacion')
        nivel = Sanitizer.getval(form, 'nivel')
        email = Sanitizer.getval(form, 'email')

        # Sanear datos
        usuario = Filter.sanitize_string(usuario)
        denominacion = Filter.sanitize_string(denominacion)
        nivel = Filter.sanitize_int(nivel)
        email = Filter.sanitize_string(email).replace(" ", "")

        # Errores
        errores = {}

        # Validar que usuario no este repetido
        user_exist = self.model.get_uid(UsuarioHelper.set_uid(usuario))
        if len(user_exist) > 0:
            errores['usuario'] = "El nombre de usuario ya existe"

        if errores:
            self.agregar(errores)

        # Insert
        self.model.usuario_id = UsuarioHelper.set_uid(usuario)
        self.model.denominacion = denominacion
        self.model.nivel = nivel
        self.model.email = email
        self.model.insert()
        credencial = CredencialController()
        credencial.guardar(form)

        url = '/usuario/agregar'
        message = 'Usuario guardado correctamente'
        print self.view.render_wait(url, 1, message)

    def get(self, uid):
        self.model.usuario_id = Filter.sanitize_string(uid)
        self.model.select()



class UsuarioHelper(object):

    @staticmethod
    def set_uid(usuario):
        user_hash = sha512(usuario).hexdigest()
        x = user_hash[240:266]
        salt = md5('{}{}'.format(x, user_hash)).hexdigest()
        return md5('{}{}'.format(user_hash, salt)).hexdigest()

