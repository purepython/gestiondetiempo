#-*- coding: utf-8 -*-
import smtplib
from cgi import FieldStorage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template

from core.collector import CollectorObject
from core.headers import header
from core.helper import read_file, Sanitizer, Sanitizer
from core.mvc.view import View
from core.sessions import Sessions
from settings import HTTP_HTML
from settings import PRIVATE_DIR, HTTP_HTML, ARG


class Admin(object):
    pass


class AdminView(View):

    def inicio(self):
        html = read_file('back/inicio')
        print self.render_template('Gestión de tiempo', html, 'back')

    def wait(self, url, menssage):
        print self.render_wait(url, 2, menssage)

class AdminController(object):

    def __init__(self):
        self.model = Admin()
        self.view = AdminView()

    def intro(self):
        url = '/admin/inicio'
        message = 'Bienvenid@'
        self.view.wait(url, message)

    def inicio(self):
        self.view.inicio()

