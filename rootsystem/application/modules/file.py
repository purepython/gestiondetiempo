from os import environ
from subprocess import Popen, PIPE
from core.sessions import Sessions
from settings import PRIVATE_DIR, ARG, HTTP_HTML


class File(object):
    pass


class FileView(object):
    pass


class FileController(object):

    def __init__(self):
        self.view = FileView()

    def ver(self):
        Sessions.check()
        if not 'HTTP_REFERER' in environ:
            Sessions.check_level(1)

        filename = "{}/{}".format(PRIVATE_DIR, ARG)
        mime = FileHelper.get_mime(filename)
        print "Content-type: {}".format(mime)
        print ""
        with open(filename, 'r') as f:
            content = f.read()
        print content

    def gi77(self):
        path = '{}/entrega/gi77/'.format(PRIVATE_DIR)
        filename = '{}/{}'.format(path, ARG)
        mime = FileHelper.get_mime(filename)
        print "Content-type: {}".format(mime)
        print ""
        with open(filename, 'r') as f:
            content = f.read()
        print content

    def julissa(self):
        path = '{}/entrega/julissa'.format(PRIVATE_DIR)
        filename = '{}/{}'.format(path, ARG)
        mime = FileHelper.get_mime(filename)
        print "Content-type: {}".format(mime)
        print ""
        with open(filename, 'r') as f:
            content = f.read()
        print content

    def evento(self):
        if not 'HTTP_REFERER' in environ:
            Sessions.check_level(1)
        path = '{}/evento'.format(PRIVATE_DIR)
        filename = '{}/{}'.format(path, ARG)
        mime = FileHelper.get_mime(filename)
        print "Content-type: {}".format(mime)
        print ""
        with open(filename, 'r') as f:
            content = f.read()
        print content

    def entrega(self):
        if not 'HTTP_REFERER' in environ:
            Sessions.check_level(1)
        path = '{}/entrega'.format(PRIVATE_DIR)
        filename = '{}/{}'.format(path, ARG)
        mime = FileHelper.get_mime(filename)
        print "Content-type: {}".format(mime)
        print ""
        with open(filename, 'r') as f:
            content = f.read()
        print content


class FileHelper(object):

    @staticmethod
    def get_mime(filename):
        p = Popen(['file', filename, '--mime-type'], stderr=PIPE, stdout=PIPE)
        mime = p.stdout.read().split(': ')[1][:-1]
        return mime
